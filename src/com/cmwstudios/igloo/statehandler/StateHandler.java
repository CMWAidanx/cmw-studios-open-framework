package com.cmwstudios.igloo.statehandler;

import com.cmwstudios.igloo.CMWSF;
import com.cmwstudios.igloo.Default;
import com.cmwstudios.igloo.Framework;
import com.cmwstudios.igloo.SoftwareState;
import com.cmwstudios.igloo.crashhandler.CrashHandler;

public class StateHandler extends CMWSF implements Framework {
    public StateHandler(SoftwareState s) {
        switch (s) {
            case STOPPED:
                // Why do anything? It's already stopped!
            case STARTING:
                // Time to start the software!
                // Put whatever you need to start the software here.
                try {
                    Default.start();
                } catch (Exception e) {
                    CrashHandler c = new CrashHandler(e, SoftwareState.STARTFAILED);
                }
            case STARTFAILED:
                System.out.println("Start Failed.");
            case RUNNING:

        }
    }
}
