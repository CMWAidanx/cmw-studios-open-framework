package com.cmwstudios.igloo;

public interface Framework {
    String softwareName = "CMW Studios Framework";
    String softwareVersion = "AS-3.0.";
}
