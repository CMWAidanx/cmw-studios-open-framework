package com.cmwstudios.igloo;

import com.cmwstudios.igloo.statehandler.StateHandler;

import java.time.LocalDateTime;

public class CMWSF implements Framework {
    public static SoftwareState state = SoftwareState.STOPPED;

    private static int hour = LocalDateTime.now().getHour();
    public CMWSF() {
    }

    public static boolean recovered = false;
    public static void start() {
        System.out.println("CMW Studios Software");
        if (hour == 0) {
            System.out.println("It's a new day, let's make it a great one.");
        } else if (hour < 12) {
            System.out.println("Good Morning.");
        } else if (hour == 12) {
            System.out.println("It's high noon.");
        } else if (hour < 18) {
            System.out.println("Good afternoon!");
        } else if (hour < 21) {
            System.out.println("Good Evening.");
        } else if (hour > 21) {
            System.out.println("Good Night.");
        } else {
            System.out.println("Hi. Your clock is invalid. You should probably fix that. Or, maybe I'm broken? Or maybe it's Java? IDK, just check your clock mate!");
        }
        System.out.println("Starting software " + softwareName + "...");
        state = SoftwareState.STARTING;
        while (true) {
            StateHandler sh = new StateHandler(state);
            sh = null;
        }
    }
}
