package com.cmwstudios.igloo;

public enum SoftwareState {
    STOPPED, ABORTED, STARTING, RUNNING, STOPPING, RESTARTING, STARTFAILED
}
