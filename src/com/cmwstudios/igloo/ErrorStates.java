package com.cmwstudios.igloo;

public enum ErrorStates {
    STARTFAILED, ABORTED
}
